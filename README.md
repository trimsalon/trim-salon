Trim Salon offers women's and men's waxing, threading and tinting, along with vajuvenation, lashes and makeup services to clients in Chicago's Lincoln Park and Wicker Park neighborhoods. Their licensed and highly trained waxing technicians take pride in making your visit as comfortable and customized as possible, from consultation to after wax care.

Website : https://www.trimwax.com/